#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include "time_meas.h"
#include <time.h>

#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>
 #define N 1000
 #define update(arr,pos,val) do{arr[pos][0]=val[0];arr[pos][1]=val[1];}while(0) 
typedef struct {

    int x1;
    int x2;
    int y1;
    int y2;
    int y3;
}record_t;
int data[1000][4];	
const int zero=0;
const int one=1;
const int two=2;
const int three=3;
const int four=4;
const int five=5;
const int six=6;
const int seven=7;

void merge(int arr[][2], int p, int q, int r) {

  int n1 = q - p + 1;
  int n2 = r - q;
  float L[n1][2], M[n2][2];

  for (int i = 0; i < n1; i++)
  update(L,i,arr[p+i]);
    //L[i] = arr[p + i];
  for (int j = 0; j < n2; j++)
  update(M,j,arr[q+1+j]);
    //M[j] = arr[q + 1 + j];

  int i, j, k;
  i = 0;
  j = 0;
  k = p;

  while (i < n1 && j < n2) {
    if (L[i][0] <= M[j][0]) {
    update(arr,k,L[i]);
      //arr[k] = L[i];
      i++;
    } else {
    update(arr,k,M[j]);
      //arr[k] = M[j];
      j++;
    }
    k++;
  }

  while (i < n1) {
  update(arr,k,L[i]);
    //arr[k] = L[i];
    i++;
    k++;
  }

  while (j < n2) {
  update(arr,k,M[j]);
    //arr[k] = M[j];
    j++;
    k++;
  }
}

void mergeSort(int arr[][2], int l, int r) {
  if (l < r) {
    int m = l + (r - l) / 2;

    mergeSort(arr, l, m);
    mergeSort(arr, m + 1, r);
    merge(arr, l, m, r);
  }
}

    int comp (const void * elem1, const void * elem2) 
{
    int f = *((int*)elem1);
    int s = *((int*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

void train(){
	 FILE *fp;
    fp = fopen("random_0_1.csv", "r");

    size_t count = 0;
    for(int i =0; i<1000;i++) {
      data[i][0]=i;  //set index
    }
	
	char buffer[80];
    while (fgets(buffer, 80, fp)) {
        char *token = strtok(buffer, ";");
        
        while(token) {  
            int n = atoi(token);
            data[count/3][count%3+1]=n;
            token = strtok(NULL, ";");
            count++;
            
            
        } 

}

	fclose(fp);

}

int test(int X_new[2], int k) {
	int distances_indices[1000][2] ;

	for (int i=0;i<(1000/8);i++) {

        __m128i x_0=_mm_set_epi16(data[i*8+7][1],data[i*8+6][1],data[i*8+5][1],data[i*8+4][1] , data[i*8+3][1],data[i*8+2][1],data[i*8+1][1],data[i*8][1]) ; //create x inputs
        __m128i x_1=_mm_set_epi16(data[i*8+7][2],data[i*8+6][2],data[i*8+5][2],data[i*8+4][2] , data[i*8+3][2],data[i*8+2][2],data[i*8+1][2],data[i*8][2]) ; //create y inputs
       
        __m128i Xnew0=_mm_set_epi16(X_new[0],X_new[0],X_new[0],X_new[0], X_new[0],X_new[0],X_new[0],X_new[0]) ; //create Xnew (x-axis)
        __m128i Xnew1=_mm_set_epi16(X_new[1],X_new[1],X_new[1],X_new[1], X_new[1],X_new[1],X_new[1],X_new[1]) ; //create Xnew(y-axis)

        __m128i squarex= _mm_mullo_epi16(_mm_sub_epi16(Xnew0,x_0), _mm_sub_epi16(Xnew0,x_0)) ; //compute the distance (x-axis)

        __m128i squarey= _mm_mullo_epi16(_mm_sub_epi16(Xnew1,x_1), _mm_sub_epi16(Xnew1,x_1)) ; //compute the distance (y-axis)
        
        __m128i distance=_mm_add_epi16(squarex,squarey);

        distances_indices[i*8][1]=data[i*8][0];   //create the indices
        distances_indices[i*8+1][1]=data[i*8+1][0];
        distances_indices[i*8+2][1]=data[i*8+2][0];
        distances_indices[i*8+3][1]=data[i*8+3][0];
        distances_indices[i*8+4][1]=data[i*8+4][0];
        distances_indices[i*8+5][1]=data[i*8+5][0];
        distances_indices[i*8+6][1]=data[i*8+6][0];
        distances_indices[i*8+7][1]=data[i*8+7][0];


        distances_indices[i*8][0]=  _mm_extract_epi16(distance,zero) ;  //extract the distances to perform the sort
        distances_indices[i*8+1][0]= _mm_extract_epi16(distance,one) ;
        distances_indices[i*8+2][0]= _mm_extract_epi16(distance,two) ;
        distances_indices[i*8+3][0]= _mm_extract_epi16(distance,three) ; 
        distances_indices[i*8+4][0]=  _mm_extract_epi16(distance,four) ;
        distances_indices[i*8+5][0]= _mm_extract_epi16(distance,five) ;
        distances_indices[i*8+6][0]= _mm_extract_epi16(distance,six) ;
        distances_indices[i*8+7][0]= _mm_extract_epi16(distance,seven) ; 

	}
	

    mergeSort(distances_indices, 0, 999);
    int y_hat=0;
        
	for (int i=0;i<k;i++) {
	int arg = (int)distances_indices[i][1];
      y_hat+=data[arg][3];
    }
    y_hat/=k;


    if (y_hat > 1/2) y_hat=1;
    else y_hat=0;
    return y_hat;
	}
  


int main(){
   //TRAINING 

   train();
    //TESTING

    //SINGLE PREDICTION
    
    int x1_int = 6;
    int x2_int = 5;
    int X_new[]={x1_int,x2_int};
    int k =1;

    printf("Prediction for inputs %d,%d : %d \n",x1_int, x2_int, test(X_new, k) ); 

    //Time measurement
    /*   
    clock_t t;
    t = clock();
    for (int i=0;i<N;i++) {
    int x1_int = rand() % 20;
    int x2_int = rand() % 20;
    
    int X_new[]={x1_int,x2_int};
    test(X_new, k); 
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("took %f seconds to execute \n", time_taken); */
  return 0;
        }
