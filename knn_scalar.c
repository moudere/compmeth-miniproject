#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
//#include "time_meas.h"
#include <time.h>

#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>
 #define N 1000
 #define update(arr,pos,val) do{arr[pos][0]=val[0];arr[pos][1]=val[1];}while(0) 
typedef struct {

    int x1;
    int x2;
    int y1;
    int y2;
    int y3;
}record_t;
int data[1000][4];	
const int zero=0;
const int one=1;
const int two=2;
const int three=3;

float distance(int x1[], int x2[]) {
	float sum=0;
	int len = sizeof (x1) / sizeof (x1[0]);
	for(int i=0;i<len ;i++) {
		sum+=(x1[i]-x2[i])*(x1[i]-x2[i]);
        
	}
	return sum;
}

void merge(float arr[][2], int p, int q, int r) {

  int n1 = q - p + 1;
  int n2 = r - q;
  float L[n1][2], M[n2][2];

  for (int i = 0; i < n1; i++)
  update(L,i,arr[p+i]);
    //L[i] = arr[p + i];
  for (int j = 0; j < n2; j++)
  update(M,j,arr[q+1+j]);
    //M[j] = arr[q + 1 + j];

  int i, j, k;
  i = 0;
  j = 0;
  k = p;

  while (i < n1 && j < n2) {
    if (L[i][0] <= M[j][0]) {
    update(arr,k,L[i]);
      //arr[k] = L[i];
      i++;
    } else {
    update(arr,k,M[j]);
      //arr[k] = M[j];
      j++;
    }
    k++;
  }

  while (i < n1) {
  update(arr,k,L[i]);
    //arr[k] = L[i];
    i++;
    k++;
  }

  while (j < n2) {
  update(arr,k,M[j]);
    //arr[k] = M[j];
    j++;
    k++;
  }
}

void mergeSort(float arr[][2], int l, int r) {
  if (l < r) {
    int m = l + (r - l) / 2;

    mergeSort(arr, l, m);
    mergeSort(arr, m + 1, r);
    merge(arr, l, m, r);
  }
}

void train(){
	 FILE *fp;
    fp = fopen("random_0_1.csv", "r");
    size_t count = 0;
    for(int i =0; i<999;i++) {
      data[i][0]=i;  //set index
    }
	
	char buffer[80];
    while (fgets(buffer, 80, fp)) {
        char *token = strtok(buffer, ";");
        
        while(token) {  
            int n = atoi(token);
            data[count/3][count%3+1]=n;
            token = strtok(NULL, ";");
            count++;
            
            
        } 

}

	fclose(fp);

}

int test(int X_new[2], int k) {
	float distances[1000][2] ;

	for (int i=0;i<999;i++) {
	int x[]={data[i+1][1] , data[i+1][2] };
	distances[i][0]=distance(X_new,x );
  distances[i][1]=data[i+1][0];  
	}

    mergeSort(distances, 0, 999);
    int y_hat=0;
        
	for (int i=0;i<k;i++) {
	int arg = (int)distances[i][1];
      y_hat+=data[arg][3];
    }
    y_hat/=k;
    
    if (y_hat > 1/2) y_hat=1;
    else y_hat=0;
    return y_hat;
	}
  


int main(){
   //TRAINING  
   clock_t t;
    t = clock();
   train();
        
    //TESTING
   
    int x1_int = 6;
    int x2_int = 5;
    int X_new[]={x1_int,x2_int};
    int k=1;

    // printf("prediction : %d \n",test(X_new, 1) );
    printf("Prediction for inputs %d, %d: %d", x1_int, x2_int, test(X_new, k));

    //Time measurement
    /*
    for (int i=0;i<N;i++) {
    int x1_int = rand() % 20;
    int x2_int = rand() % 20;
    

    int X_new[]={x1_int,x2_int};
    test(X_new, k); 
    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
    printf("took %f seconds to execute \n", time_taken); 
    } 
    */
   return 0;
    
        }
