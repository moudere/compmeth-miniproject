# -*- coding: utf-8 -*-
from scipy.spatial import distance # you can select the euclidean distance
from scipy import stats #
import sys
import numpy as np
from matplotlib import pyplot as plt
import math
import random
import pandas as pd
import time
from sklearn.neighbors import KNeighborsClassifier
 
df=pd.read_excel("random_0_1.xlsx")
#print(df)
X=df[['x','y']].to_numpy()
y=df['label'].to_numpy()

          

class KNN:
    '''
    k nearest neighboors algorithm class
    __init__() initialize the model
    train() trains the model
    predict() predict the class for a new point
    '''

    def __init__(self, K):
        '''
        INPUT :
        - K : is a natural number bigger than 0
        '''
        # empty initialization of X and y
        self.X = []
        self.y = []
        # K is the parameter of the algorithm representing the number of neighborhoods
        self.k = K

    def train(self,X,y):
        '''
        INPUT :
        - X : is an array containing the features
        - y : is an array containing the labels
        '''
        self.X=X # copy your training points
        self.y=y

    def predict(self,X_new):
        '''
        INPUT :
        - X_new : is an array containing the features of new pixels whose label has to be predicted

        OUTPUT :
        - y_hat : is an array containing the predicted labels for the X_new points
        '''
        y_hat = []
        X=self.X
        y=self.y
        N=len(self.X)
        k=self.k
        
        
        for x in X_new:
            distance = []
            voisin = []
            for i in range (N) :
                p=X[i]
                distance.append([(x[0] - p[0])**2+ (x[1] - p[1])**2,i])

            distance.sort()
            
            y_hatx=0
            for i in range(k) :
                y_hatx+=y[distance[i][1]]

            y_hatx/=k
            y_hat.append(y_hatx)

        return y_hat


knn = KNN(K=5)
knn.X = X
knn.y = y

X_new=np.random.randint(20,size=(100, 2))

#Time measuring for my algorithm
start=time.time()

outputs_my = knn.predict(X_new) 
end = time.time()
measurement_my=end - start



print('Time for my kNN algorithm (100 test inputs, k=5):', measurement_my, 's')

#Time measuring for scikit learn algorithm
start = time.time()

knn_sklearn = KNeighborsClassifier(n_neighbors=5)
knn_sklearn.fit(X,y)
outputs_sk = knn_sklearn.predict(X_new)

end = time.time()
measurement_sk=end - start


end = time.time()
measurement = end - start
print('Time for sklearn kNN algorithm (100 test inputs, k=5):', measurement_sk, 's')

